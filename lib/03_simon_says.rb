def echo(str)
  str
end

def shout(str)
  str.upcase
end

def repeat(str, times = 2)
  result = str
  (times - 1).times do
    result += " " + str
  end
  result
end

def start_of_word(str, letters)
  str[0...letters]
end

def first_word(str)
  str.split(" ")[0]
end

def titleize(str)
  words = str.split(" ")
  result = words[0].capitalize
  words.drop(1).each do |word|
    if ["the", "and", "over"].include?(word)
      result += " " + word.downcase
    else
      result += " " + word.capitalize
    end
  end
  result
end
