def translate_word(word)
  vowels = /((?<!q)u|[aeioy])/
  firstvowel = word.index(vowels)
  workingword = word
  if firstvowel != 0
    workingword = word[firstvowel..-1] + word[0..firstvowel - 1]
  end
  workingword + ay
end

def translate(str)
  words = str.split(" ")
  result = translate_word(words[0])
  words.drop(1).each do |word|
    result += " " + translate_word(word)
  end
  result
end
