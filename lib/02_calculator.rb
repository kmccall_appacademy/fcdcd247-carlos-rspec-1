def add(a, b)
  a + b
end

def subtract(a, b)
  a - b
end

def sum(arr)
  total = 0
  arr.each {|x| total += x }
  total
end
